'use strict';

const path = require('path');
global.Promise = require('bluebird');
const readline = require('readline-sync');
const chalk = require('chalk');
const validator = require('validator');
const fs = require('fs-extra');
const to = require('await-fn');


const allowedCoins = ['10 cents', '50 cents', '$1', '$2', '$5'];
const drinkMap = new Map([
	['CD1', { name: 'Coke', price: 0.5 }],
	['CD2', { name: 'Sprite', price: 0.6 }],
	/** For future upgrades */
	// ['HD1', { name: 'Coffee', price: 3.6 }],
	// ['HD2', { name: 'Tea', price: 2.0 }],
]);

const log = console.log.bind(console);
const error = console.error.bind(console);
const isNumeric = numStr => !!(+numStr) || (+numStr === 0);
const logDrink = (drink, completed = true) =>
	fs.ensureFile(path.join(__dirname, '../logs/drinks.log'))
		.then(() =>
			fs.appendFile(
				path.join(__dirname, '../logs/drinks.log'),
				`${JSON.stringify(drink)}, ${(completed)? 'completed' : 'cancelled'} @ ${new Date()}\n`
			)
				.catch(error)
		)
		.catch(error);

const vend = async () => {
	while(true) {
		let coin = '', drinkObj = {};
		let [err, data] = await to(readline.question, { params: [
			`\nWelcome to my vending machine! (press ${chalk.red('enter')} to start)\n`,
			{ hideEchoBack: true, mask: '' }
		] });
		if(err) {
			error(e);
			process.exit(1);
		}

		drinkMap.forEach((value, key) => {
			log(`For ${chalk.green(value.name)} press: ${chalk.red(key)} ($${value.price})`);
		});
		log(`To cancel press ${chalk.red('0')} at any moment.\n`);

		drinkObj.selectedDrink = await to(
			readline.question,
			{ param: `Please select a drink (by its code): `, onlyData: true }
		).then(data => data.toUpperCase());

		while(!drinkMap.has(drinkObj.selectedDrink)) {
			if(drinkObj.selectedDrink === '0') break;
			drinkObj.selectedDrink = await to(
				readline.question, {
					param: `Please select a ${chalk.red('valid')} drink code: `,
					onlyData: true
				}
			).then(data => data.toUpperCase());
		}
		if(drinkObj.selectedDrink === '0') {
			await logDrink(drinkObj, false);
			continue;
		}
		log(`You've selected ${chalk.green(drinkMap.get(drinkObj.selectedDrink).name)} at ${chalk.red('$' + drinkMap.get(drinkObj.selectedDrink).price)}`);

		if(drinkObj.selectedDrink.charAt(0) === 'H') {
			const addSugar = await to(
				readline.keyInYN,
				{ param: 'Do you want sugar? ', onlyData: true }
			)
			if(addSugar) {
				drinkObj.sugarSpoons = await to(
					readline.question, {
						param: `How many spoons of sugar would you like? `,
						onlyData: true
					}
				)
				while(!isNumeric(drinkObj.sugarSpoons)) {
					if(drinkObj.sugarSpoons === '0') break;
					drinkObj.sugarSpoons = await to(
						readline.question, {
							param: `Enter a ${chalk.red('valid')} number of spoons: `,
							onlyData: true
						}
					);
				}
				if(drinkObj.sugarSpoons === '0') {
					await logDrink(drinkObj, false);
					continue;
				}
			}
		}

		drinkObj.payment = await to(readline.keyInSelect, {
			params: [['Credit Card', 'Cash'], 'Please select a payment option: '],
			onlyData: true
		});
		if(drinkObj.payment === -1) {
			await logDrink(drinkObj, false);
			continue;
		}
		if(drinkObj.payment === 0) {
			drinkObj.cardNumber = await to(readline.question, {
				param: 'Enter your credit card number: ',
				onlyData: true
			});
			while(!validator.isCreditCard(drinkObj.cardNumber)) {
				if(drinkObj.cardNumber === '0') break;
				drinkObj.cardNumber = await to(readline.question, {
					param: `Enter a ${chalk.red('valid')} credit card number: `,
					onlyData: true
				});
			}
			if(drinkObj.cardNumber === '0') {
				await logDrink(drinkObj, false);
				continue;
			}
			log('Please wait while your card is being approved...');
			await logDrink(drinkObj);
			log('Here you go!\n');
		}
		if(drinkObj.payment === 1) {
			drinkObj.coinSum = 0;
			while(drinkObj.coinSum < drinkMap.get(drinkObj.selectedDrink).price) {
				coin = await to(readline.keyInSelect, {
					params: [allowedCoins, 'Please select a coin: '],
					onlyData: true
				});
				if(coin === -1) break;
				switch(coin) {
					case 0:
						drinkObj.coinSum += 0.1;
						log('10 cents inserted.');
						break;
					case 1:
						drinkObj.coinSum += 0.5;
						log('50 cents inserted.');
						break;
					case 2:
						drinkObj.coinSum += 1;
						log('$1 inserted.');
						break;
					case 3:
						drinkObj.coinSum += 2;
						log('$2 inserted.');
						break;
					case 4:
						drinkObj.coinSum += 5;
						log('$5 inserted.');
						break;
					default:
						log('No coin inserted.');
						break;
				}
			}
			if(coin === -1) {
				log(`Here is your $${drinkObj.coinSum} back.`);
				await logDrink(drinkObj, false);
				continue;
			}
			log(`$${drinkObj.coinSum} has been inserted in total.`);
			if(drinkObj.coinSum > drinkMap.get(drinkObj.selectedDrink).price)
				log(`Here is your $${drinkObj.coinSum - drinkMap.get(drinkObj.selectedDrink).price} in change.`);
				await logDrink(drinkObj);
			log('Here you go!\n');
		}
	}
};

vend();
