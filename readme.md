# Design a solution for a beverage vending machine, and implement it

## The flow is as described below:
- The user chooses a beverage he would like to buy and the machine shows its price
- There are 2 payment methods, credit card and coins
	- For coins: the machine accept only 10 cent / 50 cent / $1 / $2 / $5. If needed, the machine will return change.
	- For credit card: an approval from the company is needed
- After the payment is approved, the user gets the selected beverage

## Important notes:
- The user can cancel the operation at each step, except after credit card approval received from the cc company.
- Today, the machine sells only light beverage (Coke/Sprite).

The vendor forecast is to sell hot beverage as well (Coffee & tea). in this case, the user will have to choose whether he want sugar or not and how many.

## We expect the following:
- A working solution that is demoable
- No UI required
- No need to handle production readiness or deployment scripts
- NodeJS, preferable ES6
	- No limitation on open-sources
- Follow design best practices
- The design should be scalable, to consider real life scenario, where
	- There are multiple machines spread all over the country
	- Statistics are collected to learn about consumer behavior and technical difficulties
